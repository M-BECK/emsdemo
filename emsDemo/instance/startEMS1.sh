#!/bin/bash
echo give right to the docker user to access through the share volume
chown -R $DOCKERUSER:$DOCKERGROUP /emshared/instance/ems
chmod 755 -R /emshared/instance/ems
echo start EMS Instance
runuser -l $DOCKERUSER -c "$EMS_HOME/bin/tibemsd -config /emshared/instance/ems/emsServer1/config/primary/tibemsd.conf"